#include <sourcemod>
#include <sdktools>
#include <sdkhooks>
#include <emitsoundany>

#define EVENT_PARAMS Handle event, const char[] name, bool dontBroadcast
#define EVENT_GET_PLAYER GetClientOfUserId(GetEventInt(event, "userid"));
#define VERSION "1.0.0"
#define MODEL_BEAM		"materials/sprites/purplelaser1.vmt"
#define MDL_LASER "materials/sprites/purplelaser1.vmt"
#define MDL_MINE "models/lasermine/lasermine.mdl"
#define PLUGIN_NAME "neatek-grenade-redux.smx"

#define LASER_COLOR_T	"255 0 0"
#define LASER_COLOR_CT	"0 0 255"
#define LASER_COLOR_D	"38 251 42"

#define LASER_WIDTH		16.0//0.12
#define EXPLODE_SOUND	"ambient/explosions/explode_8.mp3"

#define SITUATION1 Float:{90.0,0.0,0.0} // низ
#define SITUATION2 Float:{0.0,90.0,0.0} // на меня
#define SITUATION3 Float:{0.0,0.0,90.0} // лево
#define SITUATION4 Float:{-90.0,0.0,0.0} // вверх
#define SITUATION5 Float:{0.0,-90.0,0.0} // от меня
#define SITUATION6 Float:{0.0,180.0,0.0} // право

//int g_CollisionOffset = -1;
//ConVar g_hHudPosX, g_hHudPosY, g_hHudText, g_hHudColor, g_hAuthor;
ConVar g_hAuthor;
/*new g_LightningSprite;   
new g_SteamSprite;*/
int g_ExplosionSprite;
int g_SmokeSprite;


public Plugin myinfo =
{
	name = PLUGIN_NAME,
	author = "Neatek",
	description = "Simple plugin for grenade",
	version = VERSION,
	url = "https://github.com/neatek/"
};

public void OnPluginStart() {
	//g_CollisionOffset  = FindSendPropInfo("CBaseEntity", "m_CollisionGroup");
	g_hAuthor = CreateConVar("sm_plugin_author", "neatek", "Author: Neatek, www.neatek.ru", FCVAR_SPONLY|FCVAR_REPLICATED|FCVAR_NOTIFY);
	//HookEvent("player_spawn", Event_SpawnPlayer);

	//g_hHudPosX = CreateConVar("neatek_zhud_x", "0.2", "Позиция текста на экране по горизонтали (от 0.0 до 0.9)", _, true, 0.01, true, 0.9 );
	//g_hHudPosY = CreateConVar("neatek_zhud_y", "0.05", "Позиция текста на экране по вертикали (от 0.0 до 0.9)", _, true, 0.01, true, 0.9);
	//g_hHudColor = CreateConVar("neatek_zhud_color", "219 155 76", "Цвет текста, обязательно 3 значения через пробел один, пример - 219 155 76");
	//g_hHudText = CreateConVar("neatek_zhud_text", "[Создатель] neatek{new}[Группа Вк] vk.com/neatek{new}[Меню сервера на клавишу 'E']{new}[VIP на месяц 100р]{new}[Админка на месяц 250р]", "Здесь можно написать дальшейшие строки для менюшки, {new} - обозначение новой строки");
	
	/** CHECK PLUGIN NAME **/
	char s_path[512];
	BuildPath(Path_SM, s_path, sizeof(s_path), "plugins/%s", PLUGIN_NAME);
	if(!FileExists(s_path)) {
		SetFailState("Do not rename Neatek plugin! Rename it back to - '%s'.", PLUGIN_NAME);
	}

	HookEntityOutput("env_beam", "OnTouchedByEntity", OnTouchedByEntity);
	//AutoExecConfig(true, "neatek-grenade-redux");
}

public void OnMapStart() {
	PrecacheModel(MDL_MINE, true);
	PrecacheModel(MDL_LASER, true);
	PrecacheModel( MODEL_BEAM, true );
	
	PrecacheSoundAny( EXPLODE_SOUND, true );
	g_SmokeSprite = PrecacheModel( "sprites/steam1.vmt" );
	g_ExplosionSprite = PrecacheModel( "sprites/blueglow2.vmt" );
/*	AddFileToDownloadsTable( "sound/ambient/explosions/explode_8.mp3" );
	
	AddFileToDownloadsTable( "materials/sprites/blueglow2.vtf" );
	AddFileToDownloadsTable( "materials/sprites/blueglow2.vmt" );
	
	AddFileToDownloadsTable( "materials/sprites/steam2.vtf" );
	AddFileToDownloadsTable( "materials/sprites/steam2.vmt" );*/

	char s_author[48];
	g_hAuthor.GetString(s_author, sizeof(s_author));
	if(strcmp(s_author, "neatek") != 0) {
		SetFailState("Set ConVar 'sm_plugin_author' back to - 'neatek'! It's a copyright (c) 2017");
	}
}

// Function for check is valid or not player
public bool IsCorrectPlayer(int client) {
	if(client > 4096) {
		client = EntRefToEntIndex(client);
	}
		
	if( (client < 1 || client > MaxClients) || !IsClientConnected(client) ||  !IsClientInGame( client ) ) {
		return false;
	}
	
	if(IsFakeClient(client) || IsClientSourceTV(client)) {
		return false;
	}
	
	return true;
}

public Action:ChangeGrenadeDamage(Handle:hTimer, any:iEnt)
{
	SetEntPropFloat(iEnt, Prop_Send, "m_flDamage", 999.0);
	SetEntPropFloat(iEnt, Prop_Send, "m_DmgRadius", 210.0);
	
}

public OnEntityCreated(entity, const String:classname[]) 
{ 
	//PrintToChatAll(classname);
	//SDKHook(entity, SDKHook_Touch, OnTouchs); 
	if(strcmp(classname, "hegrenade_projectile") == 0) {
		///SDKHook(entity, SDKHook_Spawn, OnTankSpawned);
		//default 5
		//SetEntProp(entity, Prop_Data, "m_CollisionGroup", 1); 
		//SDKHook(entity, SDKHook_ShouldCollide, ShouldCollide);
		CreateTimer(0.01, ChangeGrenadeDamage, entity, TIMER_FLAG_NO_MAPCHANGE);
		SDKHook(entity, SDKHook_StartTouch, OnStartTouch);

	//	char model[86];
		//GetClientModel(entity,model,sizeof(model));
		//PrintToChatAll("%s",model);
		
	}
}
/*public OnTankSpawned(tank) { 
    
}*/

/*public bool ShouldCollide(int entity, int collisiongroup, int contentsmask, bool originalResult) {
	PrintToChatAll("colgroup - %d, mask %d, entity %d", collisiongroup, contentsmask, entity);
	if(collisiongroup == 0 || collisiongroup == 8 || collisiongroup == 5) {
		return false;
	}

	return true;
}*/

public OnTouchedByEntity(const String:output[], caller, activator, Float:delay)
{
	//PrintToChatAll("BEAM START TOUCH");
/*	float iVec[3];
	GetEntPropVector(caller, Prop_Send, "m_vecOrigin", iVec);
	TE_SetupExplosion( iVec, g_ExplosionSprite, 5.0, 1, 0, 50, 40, view_as<float>({ 0.0, 0.0, 1.0 }) );
	TE_SendToAll();
	TE_SetupSmoke( iVec, g_SmokeSprite, 10.0, 3 );
	TE_SendToAll();
	EmitAmbientSoundAny( EXPLODE_SOUND, iVec, caller, SNDLEVEL_NORMAL );*/
	char name[86];
	GetEntPropString(caller, Prop_Data, "m_iName", name, sizeof(name));  
	//PrintToChatAll("%s", name);
	ReplaceString(name, sizeof(name), "neateklaser", "");
	int model = StringToInt(name);
	//PrintToChatAll("%i", model);
	if(IsValidEntity(model)) {
		//AcceptEntityInput(caller, "Kill");
		SetEntProp(model, Prop_Data, "m_nNextThinkTick", 2);
	}
	if(IsValidEntity(caller)) {
		AcceptEntityInput(caller, "KillHierarchy");
	}
}

//OnStartTouchBeam
/*public void OnStartTouchBeam(int entity, int client)
{
	PrintToChatAll("BEAM START TOUCH");
}*/

public void OnStartTouch(int entity, int client)
{
	if(client < 1) {
		float position[3];
		char s_class[32];
		GetEntPropVector(entity, Prop_Send, "m_vecOrigin", position);
		
		SetEntityMoveType(entity, MOVETYPE_NONE);
		SetEntProp(entity, Prop_Data, "m_nNextThinkTick", -1);

		int offs;
		if(!GetEntityNetClass(entity,s_class,sizeof(s_class)) || (offs = FindSendPropInfo(s_class,"m_vecMins")) == -1) { 
			return 
		}

		float mins[3], maxs[3]; 
		GetEntPropVector(entity,Prop_Send,"m_vecMaxs",maxs);
		GetEntDataVector(entity,offs,mins);

		//PrintToChatAll("mins - %f %f %f", mins[0], mins[1], mins[2]);

		//float dir[3];
		//dir = 
		

		//PrintToChatAll("X: %f ", GetDirection(entity, 0));
		//PrintToChatAll("Y: %f ", GetDirection(entity, 1));
		//PrintToChatAll("Z: %f ", GetDirection(entity, 2));

		SDKUnhook(entity, SDKHook_StartTouch, OnStartTouch);
		//PrintToChatAll("%f %f %f", position[0], position[1], position[2]);

		GetDirection(entity);
	}
}

public int ReturnReverse(int index) {
	if(index == 0) {
		return 3;
	}
	else if(index == 1) {
		return 4;
	}
	else if(index == 2) {
		return 5;
	}
	else if(index == 3) {
		return 0;
	}
	else if(index == 4) {
		return 1;
	}
	else if(index == 5) {
		return 2;
	}

	return 0;
}

public void ShowDir(int index) {

	if(index == 0) {
		PrintToChatAll("низ");
	}
	else if(index == 1) {
		PrintToChatAll("на меня");
	}
	else if(index == 2) {
		PrintToChatAll("лево");
	}
	else if(index == 3) {
		PrintToChatAll("вверх");
	}
	else if(index == 4) {
		PrintToChatAll("от меня");
	}
	else if(index == 5) {
		PrintToChatAll("право");
	}

}

public float GetDirection(entity)
{
	float results[6];
	float results_end[6][3];
	float fOrigin[3], fGround[3];

	GetEntPropVector(entity, Prop_Send, "m_vecOrigin", fOrigin);

	// низ
	TR_TraceRayFilter(fOrigin, SITUATION1, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		//CreateLaser(fOrigin, fGround, "neatek", 2);
		results[0] = GetVectorDistance(fOrigin, fGround);
		results_end[0] = fGround;
	}

	// на меня
	TR_TraceRayFilter(fOrigin, SITUATION2, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		//CreateLaser(fOrigin, fGround, "neatek", 2);
		results[1] = GetVectorDistance(fOrigin, fGround);
		results_end[1] = fGround;
	}

	// лево
	TR_TraceRayFilter(fOrigin, SITUATION3, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		//CreateLaser(fOrigin, fGround, "neatek", 2);
		results[2] = GetVectorDistance(fOrigin, fGround);
		results_end[2] = fGround;
	}

	// вверх
	TR_TraceRayFilter(fOrigin, SITUATION4, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		//CreateLaser(fOrigin, fGround, "neatek", 2);
		results[3] = GetVectorDistance(fOrigin, fGround);
		results_end[3] = fGround;
	}

	// от меня
	TR_TraceRayFilter(fOrigin, SITUATION5, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		//CreateLaser(fOrigin, fGround, "neatek", 2);
		results[4] = GetVectorDistance(fOrigin, fGround);
		results_end[4] = fGround;
	}

	// право
	TR_TraceRayFilter(fOrigin, SITUATION6, MASK_PLAYERSOLID, RayType_Infinite, TraceRayNoPlayers, entity);
	if (TR_DidHit())
	{
		TR_GetEndPosition(fGround);
		
		results[5] = GetVectorDistance(fOrigin, fGround);
		results_end[5] = fGround;
	}

	float min= results[0];
	int min_i = 0;
	for(int i=0;i<6;i++) {
		if(min > results[i]) {
			min_i = i;
			min = results[i];
		}
	}

	//ShowDir(min_i);
	int reverse = ReturnReverse(min_i);
	int client = GetEntPropEnt(entity, Prop_Data, "m_hThrower");
	char thrower[32];
	FormatEx(thrower, sizeof(thrower), "%N", client);
	CreateLaser(fOrigin, results_end[reverse], thrower, GetClientTeam(client), client, entity);
	//PrintToChatAll("min : %f | %i", min, min_i);

	return 0.0;
}

public bool TraceRayNoPlayers(entity, mask, any:data)
{
	if(entity == data || (entity >= 1 && entity <= MaxClients))
	{
		return false;
	}

	return true;
}

public CreateLaser(Float:start[3], Float:end[3], String:name[], team, int client, int entity)
{
	new ent = CreateEntityByName("env_beam");
	if (ent != -1)
	{
		//PrintToChatAll("Beam client : %N", client);
		char color[16];
		if( team == 2 ) {
			color = LASER_COLOR_T;
		}
		else if( team == 3 ) {
			color = LASER_COLOR_CT;
		}
		else {
			color = LASER_COLOR_D;
		}
		/***/
		char tmp[200], dodelete[200];



		DispatchKeyValue(entity, "spawnflags", "3");
		AcceptEntityInput(entity, "DisableMotion");
		SetEntityMoveType(entity, MOVETYPE_NONE);
		TeleportEntity(entity, start, NULL_VECTOR, NULL_VECTOR);
		SetEntProp(entity, Prop_Data, "m_nSolidType", 6);
		SetEntProp(entity, Prop_Data, "m_CollisionGroup", 5);
		FormatEx(dodelete, sizeof(dodelete), "%ineateklaser", entity);
		
		FormatEx(tmp, sizeof(tmp), "%s,Kill,,0,-1", dodelete);
		DispatchKeyValue(ent, "OnBreak", tmp);
		
		//PrintToChatAll("%s", tmp);
		TeleportEntity(ent, start, NULL_VECTOR, NULL_VECTOR);
		SetEntityModel(ent, MODEL_BEAM); // This is where you would put the texture, ie "sprites/laser.vmt" or whatever.
		SetEntPropVector(ent, Prop_Data, "m_vecEndPos", end);
		DispatchKeyValue(ent, "targetname", dodelete);
		DispatchKeyValue(ent, "damage", "0");
		DispatchKeyValue(ent, "framestart", "0");
		DispatchKeyValue(ent, "BoltWidth", "4.0");
		DispatchKeyValue(ent, "renderfx", "0");
		DispatchKeyValue(ent, "TouchType", "3"); // 0 = none, 1 = player only, 2 = NPC only, 3 = player or NPC, 4 = player, NPC or physprop
		DispatchKeyValue(ent, "framerate", "0");
		DispatchKeyValue(ent, "decalname", "Bigshot");
		DispatchKeyValue(ent, "TextureScroll", "35");
		DispatchKeyValue(ent, "HDRColorScale", "1.0");
		DispatchKeyValue(ent, "life", "0"); // 0 = infinite, beam life time in seconds
		DispatchKeyValue(ent, "StrikeTime", "1"); // If beam life time not infinite, this repeat it back
		DispatchKeyValue(ent, "LightningStart", dodelete);
		DispatchKeyValue(ent, "NoiseAmplitude", "0"); // straight beam = 0, other make noise beam
		DispatchKeyValue(ent, "Radius", "256");
		DispatchKeyValue(ent, "texture", MODEL_BEAM );
		DispatchKeyValue(ent, "spawnflags", "3");
		DispatchKeyValue(ent, "rendercolor", color );
		DispatchKeyValue(ent, "renderamt", "80");

		DispatchKeyValue(entity, "ExplodeDamage", "999.0");
		DispatchKeyValue(entity, "ExplodeRadius", "40.0");

		DispatchSpawn(ent);
		
		SetEntPropFloat(ent, Prop_Data, "m_fWidth", LASER_WIDTH); 
		SetEntPropFloat(ent, Prop_Data, "m_fEndWidth", LASER_WIDTH); 
		SetEntPropEnt(ent, Prop_Data, "m_hOwnerEntity", client); // Sets the owner of the beam
		SetEntPropEnt(entity, Prop_Data, "m_hMoveChild", ent);
		SetEntPropEnt(ent, Prop_Data, "m_hEffectEntity", entity);
		ActivateEntity(ent);

		Format(tmp, sizeof(tmp), "%s,TurnOff,,0.001,-1", dodelete);
		DispatchKeyValue(ent, "OnTouchedByEntity", tmp);
		
		Format(tmp, sizeof(tmp), "%s,TurnOn,,0.002,-1", dodelete);
		DispatchKeyValue(ent, "OnTouchedByEntity", tmp);

		//Format(tmp, sizeof(tmp), "%s,Kill,,0,-1", dodelete);
		//DispatchKeyValue(ent, "OnTouchedByEntity", tmp);
		
		AcceptEntityInput(ent, "TurnOn");

		//SDKHook(ent, SDKHook_StartTouch, OnStartTouchBeam);
	}

	return ent;
}